const express = require('express');

const router = express.Router();

module.exports = (controller) => {
  router.use('/orders/estimate', controller.estimateOrder.bind(controller));

  return router;
};
