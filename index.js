const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const config = require('./config/application.json');
const apiV1 = require('./api/v1');
const ServiceFactory = require('./services/serviceFactory');
const ControllerFactory = require('./api/v1/controllers/controllerFactory');

const app = express();
const { port, glovoDiscount } = config;
const controllerFactory = new ControllerFactory(new ServiceFactory(config), glovoDiscount);

app.use(bodyParser.json());
app.use(morgan('dev'));

app.use('/api/v1', apiV1(controllerFactory));
app.use((err, req, res, next) => {
  const response = { status: err.status || 400, message: err.message };

  res
    .status(response.status)
    .json(response);
});

app.listen(port, () => console.log(`Server is running on port ${port}`));
