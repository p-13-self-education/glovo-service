class B2bController {
  constructor(glovoDiscount, b2bService, locationService) {
    this.glovoDiscount = glovoDiscount;
    this.b2bService = b2bService;
    this.locationService = locationService;
  }

  async estimateOrder(req, res, next) {
    const { from, to } = req.body;
    try {
      const [[pickup], [delivery]] = await Promise.all([this.locationService.getLocationInfo(from), this.locationService.getLocationInfo(to)]);
      const orderParams = prepareOrderParams({ from, lat: pickup.lat, lon: pickup.lon }, { to, lat: delivery.lat, lon: delivery.lon });
      const order = await this.b2bService.estimateOrder(orderParams);
      if (order.error) {
        return next(new Error(order.error));
      }

      return res.json(this.makeDiscount(order));
    } catch (err) {
      return next(err);
    }
  }

  makeDiscount(order) {
    const { total: { amount, currency } } = order;

    return {
      total: {
        amount: amount * (1 - this.glovoDiscount),
        currency,
      },
    };
  }
}

function prepareOrderParams(pickup, delivery) {
  return {
    description: 'AfterGlovo estimate order',
    addresses: [
      {
        type: 'PICKUP',
        label: pickup.from,
        lat: pickup.lat,
        lon: pickup.lon,
      },
      {
        type: 'DELIVERY',
        label: delivery.to,
        lat: delivery.lat,
        lon: delivery.lon,
      },
    ],
  };
}

module.exports = B2bController;
