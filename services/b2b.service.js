const p = require('phin');

const routes = {
  estimateOrder: '/b2b/orders/estimate',
};

class B2bService {
  constructor(serviceUrl, auth) {
    this.serviceUrl = serviceUrl;
    this.auth = auth;
  }

  estimateOrder(order) {
    const url = `${this.serviceUrl}${routes.estimateOrder}`;
    const headers = { Authorization: this.auth };
    return p({
      url,
      headers,
      method: 'POST',
      parse: 'json',
      data: order,
    }).then(res => res.body);
  }

  static generateGlovoAuth(key, secret) {
    const authHash = Buffer.from(`${key}:${secret}`, 'utf8').toString('base64');
    return `Basic ${authHash}`;
  }
}

module.exports = B2bService;
