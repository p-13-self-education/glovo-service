const B2bController = require('./b2b.controller');

class ControllerFactory {
  constructor(serviceFactory, glovoDiscount) {
    this.serviceFactory = serviceFactory;
    this.glovoDiscount = glovoDiscount;
  }

  getB2bController() {
    return new B2bController(this.glovoDiscount, this.serviceFactory.getB2bService(), this.serviceFactory.getLocationService());
  }
}

module.exports = ControllerFactory;
