const B2bService = require('./b2b.service');
const LocationService = require('./location.service');

class ServiceFactory {
  constructor(config) {
    this.config = config;
  }

  getB2bService() {
    const { glovoAPIKey, glovoAPISecret, glovoAPIUrl } = this.config;

    return new B2bService(glovoAPIUrl, B2bService.generateGlovoAuth(glovoAPIKey, glovoAPISecret));
  }

  getLocationService() {
    const { locationAPIKey, locationAPIUrl } = this.config;

    return new LocationService(locationAPIUrl, locationAPIKey);
  }
}

module.exports = ServiceFactory;
