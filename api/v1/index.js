const express = require('express');
const b2bRouter = require('./routers/b2b.router');

const router = express.Router();

module.exports = (controllerFactory) => {
  router.use('/b2b', b2bRouter(controllerFactory.getB2bController()));

  return router;
};
