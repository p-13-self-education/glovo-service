const p = require('phin');

const routes = {
  getLocationInfo: '/v1/search.php?key={key}&q={q}&limit={limit}&format={format}',
};

class LocationService {
  constructor(serviceUrl, key) {
    this.serviceUrl = serviceUrl;
    this.key = key;
  }

  getLocationInfo(location, limit = 1, format = 'json') {
    const route = routes.getLocationInfo
      .replace('{key}', this.key)
      .replace('{q}', location)
      .replace('{limit}', limit.toString())
      .replace('{format}', format);

    return p({
      url: `${this.serviceUrl}${route}`,
      method: 'GET',
      parse: 'json',
    }).then(res => res.body);
  }
}

module.exports = LocationService;
